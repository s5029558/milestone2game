/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mines;

import java.awt.BorderLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingUtilities;


/**
 * Represents the game window
 */
public class Mines extends JFrame // 	The game board is added.
{
    private final int FRAME_WIDTH = 250; // Width of the window
    private final int FRAME_HEIGHT = 290; //High of the window
    private final JLabel statusbar; // Window status bar
    
    
    /**
     * Initialize the game window giving the value
      * Of its attributes
     */
    public Mines() 
    {

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // close of the window
        setSize(FRAME_WIDTH, FRAME_HEIGHT); // Width and height of the window
        setLocationRelativeTo(null); //initial position
        setTitle("Minesweeper"); //Title of the window

        statusbar = new JLabel(""); // create status bar
        add(statusbar, BorderLayout.SOUTH); //Position status bar in the window
        add(new Board(statusbar)); // I create and add a board to the window
        setResizable(false); // Fixed size window
        
    } // End of Mines
    
    /**
     * 
     * @param args Command line arguments
     */
    public static void main(String[] args) // 	The window is created. 
    {
        SwingUtilities.invokeLater(new Runnable() 
        {
            /**
             * JFrame run method overload
             */
            @Override
            public void run() 
            {                
                JFrame ex = new Mines();
                ex.setVisible(true);                
            } // End of run
        }); // End of the object Runnable
    } // End of main
} // End of class Mines
